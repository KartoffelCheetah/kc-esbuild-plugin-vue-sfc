# KC Esbuild Plugin Vue SFC

You are free to use this, but I do not recommend doing so. This is only for my own projects, so the code quality may be nearly poop.

Mostly based on pipe01's nice project esbuild-plugin-vue3: https://github.com/pipe01/esbuild-plugin-vue3

I didn't understand the project initially so I decided to follow the code and rewrite myself.

This also adds support for the `<script setup>` syntax based on this two projects:
* https://github.com/vuejs/core/blob/main/packages/compiler-sfc
* https://github.com/vitejs/vite-plugin-vue/blob/main/packages/plugin-vue

