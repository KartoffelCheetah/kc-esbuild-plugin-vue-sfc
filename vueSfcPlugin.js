/*
 * Mostly based on:
 * * https://github.com/pipe01/esbuild-plugin-vue3/blob/master/src/index.ts
 * * https://github.com/vuejs/core/blob/main/packages/compiler-sfc/src/compileTemplate.ts
 * * https://github.com/vitejs/vite-plugin-vue/blob/main/packages/plugin-vue/src/template.ts
 * */
import fs from 'node:fs';
import path from 'node:path';
import crypto from 'node:crypto';
import sfc from '@vue/compiler-sfc';
import resolvePath from './_resolvePath.js';

const namespace = 'vueSfc';

export default function ({ preprocessorPrefixes } = {}) {
	return {
		name: 'vueSfcPlugin',
		setup ({ onResolve, onLoad }) {

			onResolve({ filter: /.vue$/ }, (args) => ({
				path: resolvePath(args),
				namespace,
			}));

			onLoad({ filter: /.*/, namespace }, loadSfcFile);

			onResolve({ filter: /.vue\?type=/ }, (args) => {
				const usp = new URLSearchParams(args.path.slice(args.path.indexOf('?')));
				return {
					path: resolvePath(args),
					namespace: `${namespace}_${usp.get('type')}`,
					pluginData: {
						...args.pluginData,
						styleIdx: usp.get('index'),
					},
				};
			});

			onLoad({ filter: /.*/, namespace: `${namespace}_script` }, loadSfcScript);
			onLoad({ filter: /.*/, namespace: `${namespace}_template` }, loadSfcTemplate);
			onLoad({ filter: /.*/, namespace: `${namespace}_style` }, loadSfcStyle.bind(undefined, { preprocessorPrefixes }));
		},
	};
}

async function loadSfcFile (args) {
	const encPath = args.path.replace(/\\/g, '\\\\');
	const filename = path.basename(args.path);
	// scopedId calculation is copied from here https://github.com/pipe01/esbuild-plugin-vue3/blob/e33213faadb03335a16a4d2c45c1cb84f9e807b7/src/index.ts#L135C19-L135C101
	// scopeId is for CSS prefixing https://github.com/vuejs/core/blob/caeb8a68811a1b0f799632582289fcf169fb673c/packages/compiler-sfc/src/compileScript.ts#L64
	const scopeId = crypto.createHash("md5").update(filename).digest().toString("hex").substring(0, 8);
	const { descriptor: sfcParseDescriptor } = sfc.parse(
		await fs.promises.readFile(args.path, 'utf8'),
		{ filename },
	);
	const compileScriptOutput = ((sfcParseDescriptor.script || sfcParseDescriptor.scriptSetup)
		? sfc.compileScript(sfcParseDescriptor, { id: scopeId })
		: undefined
	);

	let contents = '';
	// script
	if (sfcParseDescriptor.script || sfcParseDescriptor.scriptSetup) {
		contents += `import script from "${(!sfcParseDescriptor.scriptSetup && sfcParseDescriptor.script?.src) || encPath}?type=script";\n`;
		contents += sfcParseDescriptor.script?.content || '';
	} else {
		contents += 'const script = {};\n';
	}
	// styles
	for (const styleIdx in sfcParseDescriptor.styles) {
		contents += `import "${encPath}?type=style&index=${styleIdx}";\n`;
	}
	// template
	// NOTE SSR
	// NOTE: some optimisations probably can be done here by calling ssrRender instead of render.
	contents += `import { render } from "${encPath}?type=template";\nscript.render = render;`;

	// meta
	contents += `script.__file = ${JSON.stringify(filename)}\n`;
	const isScopedStyled = sfcParseDescriptor.styles.some(s => s.scoped);
	if (isScopedStyled) {
		contents += `script.__scopeId = ${JSON.stringify(`data-v-${scopeId}`)}\n`;
	}
	contents += 'export default script;\n';
	return {
		resolveDir: path.dirname(args.path),
		watchFiles: [args.path],
		pluginData: {
			scopeId,
			sfcParseDescriptor,
			compileScriptOutput,
			isScopedStyled,
		},
		contents,
	};
}

function loadSfcScript (args) {
	const { pluginData: { compileScriptOutput } } = args;
	if (!compileScriptOutput) {
		throw new Error('sfcParseDescriptor is missing script');
	}
	const { content: scriptContent, map: scriptMap, lang: loader = 'js' } = compileScriptOutput;
	return {
		loader,
		resolveDir: path.dirname(args.path),
		contents: scriptContent + (scriptMap ? `\n\n//@ sourceMappingURL=data:application/json;charset=utf-8;base64,${Buffer.from(JSON.stringify(scriptMap)).toString('base64')}` : ''),
	};
}

function loadSfcTemplate (args) {
	const { pluginData: { compileScriptOutput, sfcParseDescriptor: { template, filename }, scopeId, isScopedStyled } } = args;
	if (!template) {
		throw new Error('sfcParseDescriptor is missing template');
	}
	const { code: contents, tips, errors } = sfc.compileTemplate({
		isProd: process.env.NODE_ENV !== 'development',
		id: scopeId,
		filename: args.path,
		scoped: isScopedStyled,
		source: template.content,
		compilerOptions: {
			scopeId: `data-v-${scopeId}`,
			bindingMetadata: compileScriptOutput.bindings,
		},
		// NOTE SSR
		// ssr: boolean,
		// ssrCssVars: [],
	});
	if (errors.length) {
		return showErrors({ args, errors, filename, template });
	}
	return {
		loader: 'ts',
		resolveDir: path.dirname(args.path),
		warnings: tips.map(t => ({ text: t })),
		contents,
	};
}

async function loadSfcStyle ({ preprocessorPrefixes }, args) {
	const { pluginData: { sfcParseDescriptor, scopeId, styleIdx } } = args;
	const style = sfcParseDescriptor.styles[styleIdx];
	const { code: contents, errors } = await sfc.compileStyleAsync({
		id: scopeId,
		filename: args.path,
		scoped: style.scoped,
		preprocessLang: style.lang,
		source: (preprocessorPrefixes?.[style.lang] || '') +  style.content,
	});
	if (errors.length) {
		return showErrors({ args, errors, style });
	}
	return {
		loader: 'css',
		resolveDir: path.dirname(args.path),
		contents,
	};
}

function showErrors ({ args, errors, filename, template, style }) {
	// This is kind of copypasta from https://github.com/pipe01/esbuild-plugin-vue3/blob/master/src/index.ts
	if (errors.length) {
		return {
			errors: errors.map((err) => {
				if (typeof err === 'string') {
					return { text: err };
				}
				if (template) {
					const { message, loc } = err;
					return {
						text: message,
						location: loc && {
							file: filename,
							line: loc.start.line + template.loc.start.line + 1,
							column: loc.start.column,
							lineText: loc.source,
						}
					};
				}
				if (style) {
					return {
						text: err.message,
						location: {
							namespace: 'file',
							file: err.file.replace(/\?.*?$/, ''),
							line: err.file === args.path ? style.loc.start.line + err.line - 1 : err.line,
							column: err.column,
						}
					};
				}
			}),
		};
	}
}
